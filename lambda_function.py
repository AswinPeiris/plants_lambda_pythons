import mysql.connector
import json

def getPlants(event):
    database = mysql.connector.connect(
        user = '',
        password = '',
        host = '',
        database = '',
        port = 3306
    )
    mycursor = database.cursor()
    sql = "SELECT * FROM plants"
    mycursor.execute(sql)
    result = mycursor.fetchall()
    mycursor.close()
    database.close()
    return{
        "statusCode": 200,
        "headers":{"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": True, "Content-Type":"application/json"},
        "body": json.dumps(result)
    }

